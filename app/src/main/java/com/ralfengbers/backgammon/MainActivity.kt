package com.ralfengbers.backgammon

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.io.File
import java.io.FileOutputStream


const val PICK_IMAGE = 1

@Serializable
data class Player(val id: Int, var score: Int = 0)

@Serializable
data class Game(val gameID: Int = 1,
                var gameName: String = "",
                val Player1: Player,
                val Player2: Player)

// see https://stackoverflow.com/questions/41928803/how-to-parse-json-in-kotlin

class MainActivity : AppCompatActivity() {

    private lateinit var games: Game
    private var playerID: Int = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        hideSystemUI(this)

        games = readJSONFile()
        setGamesData(games)

        findViewById<TextView>(R.id.score_player1).apply {
            setOnLongClickListener {
                pickScore(games.Player1)
                true
            }
        }
        findViewById<TextView>(R.id.score_player2).apply {
            setOnLongClickListener {
                pickScore(games.Player2)
                true
            }
        }
        findViewById<ImageView>(R.id.image_player1).apply {
            setOnLongClickListener {
                pickImage(games.Player1)
                true
            }
        }
        findViewById<ImageView>(R.id.image_player2).apply {
            setOnLongClickListener {
                pickImage(games.Player2)
                true
            }
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        hideSystemUI(this)
    }

    private fun hideSystemUI(activity: Activity) {
        val decorView = activity.window.decorView
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        if (Build.VERSION.SDK_INT > 18) {
            //uiState = uiState or (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_IMMERSIVE)
            decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    or View.SYSTEM_UI_FLAG_IMMERSIVE)
        }
        /*else {
            val handler = Handler()
            decorView.setOnSystemUiVisibilityChangeListener { visibility ->
                if (visibility == View.VISIBLE) {
                    val runnable = Runnable { hideSystemUI(activity, false, delayMs) }
                    if (immediate) {
                        Handler().post(runnable)
                    } else {
                        handler.postDelayed(runnable, delayMs)
                    }
                }
            }
        }*/

    }

    private fun readJSONFile(): Game {
        return with(File(filesDir, "games.json")) {
            if (!this.exists()) {
                Game(1, "", Player(1, 0), Player(2, 0)).also {
                    writeJSONFile(it)
                }
            }
            Json.parse(Game.serializer(), this.readText())
        }
    }


    private fun writeJSONFile(games: Game) {
        with(File(filesDir, "games.json")) {
            this.printWriter().use { out ->
                out.print(Json.stringify(Game.serializer(), games))
            }
        }
        setGamesData(games)
    }


    private fun setGamesData(games: Game) {
        setPlayerData(games.Player1)
        setPlayerData(games.Player2)

        when {
            games.Player1.score > games.Player2.score -> {
                val diffScore = games.Player1.score - games.Player2.score
                findViewById<TextView>(R.id.diffscore_player1).text = "+%d".format(diffScore)
                findViewById<TextView>(R.id.diffscore_player2).text = ""
            }
            games.Player1.score < games.Player2.score -> {
                val diffScore = games.Player2.score - games.Player1.score
                findViewById<TextView>(R.id.diffscore_player1).text = ""
                findViewById<TextView>(R.id.diffscore_player2).text = "+%d".format(diffScore)
            }
            else -> {
                findViewById<TextView>(R.id.diffscore_player1).text = ""
                findViewById<TextView>(R.id.diffscore_player2).text = ""
            }
        }
    }

    private fun setPlayerData(player: Player) {
        when (player.id) {
            1 -> {
                findViewById<TextView>(R.id.score_player1).text = player.score.toString()
                with(File(filesDir, getString(R.string.imageFile_player1))) {
                    if (this.exists()) {
                        findViewById<ImageView>(R.id.image_player1).setImageBitmap(
                                BitmapFactory.decodeFile(this.absolutePath)
                        )
                    }
                }
            }
            2 -> {
                findViewById<TextView>(R.id.score_player2).text = player.score.toString()
                with(File(filesDir, getString(R.string.imageFile_player2))) {
                    if (this.exists()) {
                        findViewById<ImageView>(R.id.image_player2).setImageBitmap(
                                BitmapFactory.decodeFile(this.absolutePath)
                        )
                    }
                }
            }
        }
    }

    private fun addToPlayer(player: Player, win: Int) {
        player.score += win
        writeJSONFile(games)
    }


    fun addToPlayer1(view: View) {
        addToPlayer(games.Player1, Integer.parseInt(view.tag.toString()))
    }


    fun addToPlayer2(view: View) {
        addToPlayer(games.Player2, Integer.parseInt(view.tag.toString()))
    }


    @SuppressLint("InflateParams")
    private fun pickScore(player: Player) {
        val dialogView = this.layoutInflater.inflate(R.layout.score_picker_dialog, null)
        val score = dialogView.findViewById(R.id.score_picker) as EditText

        AlertDialog.Builder(this).run {
            setView(dialogView)
            setTitle("Pick a score.")
            setNegativeButton("Cancel") { _, _: Int -> }
            setPositiveButton("OK") { _, _: Int ->
                player.score = score.text.toString().toInt()
                writeJSONFile(games)
            }
            create()
            show()
        }
    }

    private fun pickImage(player: Player) {
        playerID = player.id

        // ACTION_OPEN_DOCUMENT retains permissions to access picture
        Intent(Intent.ACTION_GET_CONTENT).apply {
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            type = "image/*"
        }.also {
            Intent.createChooser(it, "Select Image").run {
                startActivityForResult(this, PICK_IMAGE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, imageReturnedIntent: Intent?) {
        when (requestCode) {
            PICK_IMAGE -> if (resultCode == RESULT_OK) {
                val selectedImage = imageReturnedIntent?.data
                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, selectedImage)

                lateinit var filename: File

                when (playerID) {
                    1 -> {
                        filename = File(filesDir, getString(R.string.imageFile_player1))
                        findViewById<ImageView>(R.id.image_player1).setImageBitmap(bitmap)
                    }
                    2 -> {
                        filename = File(filesDir, getString(R.string.imageFile_player2))
                        findViewById<ImageView>(R.id.image_player2).setImageBitmap(bitmap)
                    }
                }

                try {
                    FileOutputStream(filename).use { out ->
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out) // bmp is your Bitmap instance
                        // PNG is a lossless format, the compression factor (100) is ignored
                    }
                } catch (e: Throwable) {
                    e.printStackTrace()
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent)
    }

}
